package jp.alhinc.sanada_hiroki;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main (String[] args) {

		//*****************************************
		//  支店定義ファイル読み込み
		//*****************************************
		BufferedReader br = null;
		List<Map<String, Object>> sales = new ArrayList<Map<String, Object>>();
		Map<String, Object> sale;

		try {
			String dirPass;			// 支店定義ファイルのディレクトリパス
			dirPass = args[0];		// コマンドライン引数使用

			// チェックのためにコンソール出力
			System.out.println("ここにあるファイルを開きます => " + dirPass );
			if(args.length != 1){		// コマンドライン引数が２以上のとき例外処理
				System.out.println("予期せぬエラーが発生しました");
				return;
			}
			File file = new File(dirPass, "branch.lst");
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;			// 読み込んだ１行を入れる変数

			while((line = br.readLine()) != null) {

				// チェックのためにコンソール出力
				System.out.println("読み込んだ文字列 --> " + line);

				String[] data = line.split(",",0);	// 行をカンマ区切りで配列に

				//  読み込んだ１行に対してのフォーマットチェック
				//	  ・要素数は２つである事
				//	  ・支店コードが数字かつ３桁である事
				if(data.length != 2 || data[0].matches("^\\d{3}$") == false){
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}

				// MAPに格納する
				sale = new HashMap<String, Object>();
				sale.put("支店コード", data[0]);
				sale.put("支店名", data[1]);
				sale.put("売上金額", (long) 0);
				sales.add(sale);
			}

			// チェックのためにコンソール出力
			System.out.println("\r\nリストの中身を表示します");
			for(int i = 0; i < sales.size(); i++) {
				System.out.println(sales.get(i));
			}
			System.out.println("---------------読み込み処理終了------------");


			//*****************************************
			//  集計処理
			//*****************************************

			// チェックのためにコンソール出力
			System.out.println("\r\n---------------売上集計処理開始------------");

			// 売上ファイルを検索し、リストを取得
			File dir = new File(dirPass);
			File[] allFiles = dir.listFiles();
			List<String> files = new ArrayList<String>();
			String fileName;
			String fileExtension = ".rcd";
			String strFileName;


			for(int i = 0; i < allFiles.length; i++){	//取得したすべてのファイルの間
				//拡張子がrcd？
				fileName = allFiles[i].getName();
				strFileName = fileName.substring(0, 8);	// 検索したい先頭８文字

				//  指定された拡張子、かつ、先頭８字が半角数字のとき
				//  数字桁.rcdだけのリストを作る
				if(fileName.endsWith(fileExtension) == true || isNumber(strFileName) == true){

					// チェックのためにコンソール出力
					System.out.println(fileName);	// 開くファイルネーム表示

					files.add(fileName);
				}
			}

			// チェックのためにコンソール出力
			System.out.println("\r\nファイルリストの中身を表示します。（ソート後）\r\n(数字８桁.rcd)");
			for(int i = 0; i < files.size(); i++) {
				System.out.println(files.get(i));
			}
			System.out.println("---------------読み込み処理終了------------");

			// ファイル名のフォーマットが正しい数字8桁かつ.rcd拡張子のファイルリストに対して行う処理
			// ファイル名が1から始まる連番ではないときは例外処理
			Collections.sort(files);
			int count = 1;
			for(int i = 0; i < files.size(); i++) {
				//rcdファイルを読む
				br = null;
				try{
					fileName = files.get(i);
					String strFileNameNum = fileName.substring(0, 8);
					if(Integer.parseInt(strFileNameNum) != count) {
						System.out.println("売上ファイルが連番になっていません。");
						return;
					}
					file = new File(dirPass, fileName);
					fr = new FileReader(file);
					br = new BufferedReader(fr);
					String shitenCode;
					long kingaku;
					long goukei;
					long jougen = 9999999999L;

					//支店コードと売上金額を保持する
					shitenCode = br.readLine();
					kingaku = Long.parseLong(br.readLine());
					// 売上ファイルの中身が３行以上の場合の例外処理
					if( br.readLine() != null ){
						System.out.println("<" + files.get(i) + ">のフォーマットが不正です。");
						return;
					}

					// チェックのためにコンソール出力
					System.out.println("支店コード - " + shitenCode + " : 売上金額 = " + kingaku);

					//支店コードが同じMAPを探す
					long uriage;
					String code;
					for(int j = 0; j < sales.size(); j++) {
						sale = sales.get(j);
						code = (String) sale.get("支店コード");
						if(code.equals(shitenCode)){
							//売上金額を加算する
							uriage = (long) sale.get("売上金額");
							goukei = uriage + kingaku;
							// 売上金額が10桁を超えたときの例外処理
							if(goukei > jougen){
								System.out.println("合計金額が10桁を超えました。");
								return;
							}
							sale.put("売上金額", goukei);
							break;
						} else if(j == sales.size() - 1){		// 支店コードが存在しなかった場合の例外処理
							System.out.println("<" + files.get(i) + ">の支店コードが不正です。");
							return;
						}
					}

				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました。");
				} finally {
					if(br != null) {
						try {
							br.close();
						} catch(IOException e) {
							System.out.println("closeできませんでした。");
						}
					}
				}
				count++;
			}

			// チェックのためにコンソール出力
			System.out.println("\r\n");
			for(int i = 0; i < sales.size(); i++)
				System.out.println(sales.get(i));
			System.out.println("\r\n---------------売上集計処理終了------------");

			//*****************************************
			//  出力
			//*****************************************
			BufferedWriter bw = null;
			try {
//				dirPass = args[0];						// コマンドライン引数（支店別集計ファイル出力先）
				file = new File(dirPass, "branch.out");
				FileWriter fw = new FileWriter(file);
				bw = new BufferedWriter(fw);

				for(int i = 0; i < sales.size(); i++){
					sale = sales.get(i);
					bw.write(sale.get("支店コード") + "," + sale.get("支店名") + "," + sale.get("売上金額") + "\r\n");
				}
			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました。");
			} finally {
				if(bw != null) {
					try {
						bw.close();
					} catch(IOException e) {
						System.out.println("closeできませんでした。");
					}
				}

				// チェックのためにコンソール出力
				System.out.println("\r\n---------------ファイル出力処理終了------------");

			}

		} catch(ArrayIndexOutOfBoundsException e){
			System.out.println("予期せぬエラーが発生しました。");
		} catch(FileNotFoundException e) {		// 支店定義ファイルが存在しないときの処理
			System.out.println("支店定義ファイルが存在しません");
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました。");
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("closeできませんでした。");
				}
			}

			//チェックのためにコンソール出力
			System.out.println("処理が終了しました");

		}
	}


	public static boolean isNumber(String num) {
		try {
	        Integer.parseInt(num);
	        return true;
	        } catch (NumberFormatException e) {
	        return false;
	    }
	}
}
