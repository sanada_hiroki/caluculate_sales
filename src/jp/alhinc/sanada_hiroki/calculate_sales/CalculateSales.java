package jp.alhinc.sanada_hiroki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main (String[] args) {
		//*****************************************
		//  支店定義ファイル読み込み
		//*****************************************
		List<Map<String, Object>> sales = new ArrayList<Map<String, Object>>();
		Map<String, Object> sale;

		// コマンドライン引数が２以上のとき例外処理
		if(args.length != 1){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		String dirPass;			// 支店定義ファイルのディレクトリパス
		dirPass = args[0];		// コマンドライン引数使用
		// 指定されたファイルを読み込んでMAPに格納
		if(readLineList(dirPass + File.separator + "branch.lst", sales) == false){
			return;
		}
		//*****************************************
		//  集計処理
		//*****************************************
		// 売上ファイルを検索し、リストを取得
		File dir = new File(dirPass);
		File[] allFiles = dir.listFiles();
		List<String> files = new ArrayList<String>();
		String fileName;
		String fileExtension = ".rcd";
		String strFileName;

		for(int i = 0; i < allFiles.length; i++){	//取得したすべてのファイルの間
			//拡張子がrcd？
			fileName = allFiles[i].getName();
			if(fileName.length() == 12) {
				strFileName = fileName.substring(0, 8);	// 検索したい先頭８文字
				//  指定された拡張子、かつ、先頭８字が半角数字のとき
				//  数字桁.rcdだけのリストを作る
				if(allFiles[i].isFile() && fileName.endsWith(fileExtension) && strFileName.matches("^\\d{8}$"))
					files.add(fileName);
			}
		}
		// ファイル名のフォーマットが正しい数字8桁かつ.rcd拡張子のファイルリストに対して行う処理
		// ファイル名が1から始まる連番ではないときは例外処理
		Collections.sort(files);
		int count = 1;
		for(int i = 0; i < files.size(); i++) {
			//rcdファイルを読む
			fileName = files.get(i);
			String strFileNameNum = fileName.substring(0, 8);
			if(Integer.parseInt(strFileNameNum) != count) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
			BufferedReader br = null;
			try{
				File file = new File(dirPass, fileName);
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);
				String shitenCode;
				String strKingaku;
				long kingaku;
				long goukei;
				long jougen = 9999999999L;
				//支店コードと売上金額を保持する
				shitenCode = br.readLine();
				strKingaku = br.readLine();
				if( shitenCode == null || strKingaku == null || br.readLine() != null){
					System.out.println(files.get(i) + "のフォーマットが不正です");
					return;
				}
				// 売上ファイル2行目が数字以外の文字を含む場合の処理
				if( strKingaku.matches("^[0-9]*$")){
					kingaku = Long.parseLong(strKingaku);
				}
				else{
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				//支店コードが同じMAPを探す
				long uriage;
				String code;
				for(int j = 0; j < sales.size(); j++) {
					sale = sales.get(j);
					code = (String) sale.get("支店コード");
					if(code.equals(shitenCode)){
						//売上金額を加算する
						uriage = (long) sale.get("売上金額");
						goukei = uriage + kingaku;
						// 売上金額が10桁を超えたときの例外処理
						if(goukei > jougen){
							System.out.println("合計金額が10桁を超えました");
							return;
						}
						sale.put("売上金額", goukei);
						break;
					} else if(j == sales.size() - 1){		// 支店コードが存在しなかった場合の例外処理
						System.out.println(files.get(i) + "の支店コードが不正です");
						return;
					}
				}
			}catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if(br != null) {
					try {
						br.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
			count++;
		}

		//*****************************************
		//  出力
		//*****************************************
		if(writeBranchOut(dirPass + File.separator + "branch.out", sales) == false ){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
	}

	public static Boolean readLineList(String strFilePass, List<Map<String,Object>> sales) {
		BufferedReader br = null;
		try {
			File file = new File(strFilePass);
			if(file.exists() == false) {
				System.out.println("支店定義ファイルが存在しません");
				return false;
			}
			br = new BufferedReader(new FileReader(file));
			String strLine;
			while((strLine = br.readLine()) != null){
				String[] data = strLine.split(",",0);	// 行をカンマ区切りで配列に

				//  読み込んだ１行に対してのフォーマットチェック
				//	  ・要素数は２つ、かつ、支店コードが数字３桁
				if(data.length != 2 || data[0].matches("^\\d{3}$") == false){
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}

				// MAPに格納する
				Map<String, Object> sale = new HashMap<String, Object>();
				sale.put("支店コード", data[0]);
				sale.put("支店名", data[1]);
				sale.put("売上金額", (long) 0);
				sales.add(sale);
			}
		} catch (IOException e){
			return false;
		} finally {
			if(br != null){
				try{
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

	public static Boolean writeBranchOut(String strFilePass, List<Map<String,Object>> sales){
		Map<String, Object> sale;
		BufferedWriter bw = null;
		try{
			File file = new File(strFilePass);
			bw = new BufferedWriter(new FileWriter(file));
			for(int i = 0; i < sales.size(); i++){
				sale = sales.get(i);
				bw.write(sale.get("支店コード") + "," + sale.get("支店名") + "," + sale.get("売上金額") + System.lineSeparator());
			}
		}catch (IOException e){
			return false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch(IOException e) {
					return false;
				}
			}
		}
		return true;
	}
}