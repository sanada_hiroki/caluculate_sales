package jp.alhinc.sanada_hiroki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class calculate_sales {
	public static void main(String[] args) {

		/*-**********************************************************
		  支店定義ファイル読み込み
		***********************************************************/
		String dirPass; // 支店定義ファイルのディレクトリパス
		dirPass = args[0]; // コマンドライン引数使用

		// // チェックのためにコンソール出力
		// System.out.println("ここにあるファイルを開きます => " + dirPass );

		BufferedReader br = null;
		List<Map<String, Object>> sales = new ArrayList<Map<String, Object>>();
		Map<String, Object> sale;

		// // チェックのためにコンソール出力
		// System.out.println("チェックのために読み込んだ文字列を１行ずつ出力");

		File file = new File(dirPass, "branch.lst");
		// if(file.exists() == true){ // 支店定義ファイルが存在しない場合の処理
		// System.out.println("支店定義ファイルが存在しません");
		// return;
		// } else {

		try {
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			while ((line = br.readLine()) != null) {

				// // チェックのためにコンソール出力
				// System.out.println(line);

				String[] data = line.split(",", 0); // 行をカンマ区切りで配列に
				// MAPに格納する
				sale = new HashMap<String, Object>();
				sale.put("支店コード", data[0]);
				sale.put("支店名", data[1]);
				sale.put("売上金額", (int) 0);
				sales.add(sale);
			}

			// // チェックのためにコンソール出力
			// for(int i = 0; i < sales.size(); i++) {
			// System.out.println(sales.get(i));
			// }

		} catch (IOException e) {
			System.out.println("エラーが発生しました。");
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("closeできませんでした。");
				}
			}
		}
//	}
	/*-**********************************************************
	  集計
	***********************************************************/

	// * 売上ファイルを検索し、リストを取得
	File dir = new File(dirPass);
	File[] files = dir.listFiles();

	for(
	int i = 0;i<files.length;i++)
	{ // 取得したすべてのファイルの間
		// 拡張子がrcd？
		String fileName = files[i].getName();
		String fileExtension = ".rcd"; // 検索したい拡張子
		String strFileName = fileName.substring(0, 8); // 検索したい先頭８文字

		if (fileName.endsWith(fileExtension) == true) { // 拡張子が.rcdのとき
			if (isNumber(strFileName) == true) { // 先頭８字が数字のとき

				// rcdファイルを読み込んで該当する支店の合計金額にそれぞれ加算する
				// // チェックのためにコンソール出力
				// System.out.println(fileName);

				// rcdファイルを読む
				// 支店コードと売上金額を保持する
				// 支店コードが同じMAPを探す
				// 売上金額を加算する
				try {
					file = new File(dirPass, fileName);
					FileReader fr = new FileReader(file);
					br = new BufferedReader(fr);
					String shitenCode;
					int kingaku;

					shitenCode = br.readLine();
					kingaku = Integer.parseInt(br.readLine());

					// // チェックのためにコンソール出力
					// System.out.println("支店コード - " + shitenCode + " : 売上金額 = "
					// + kingaku);

					for (int j = 0; j < sales.size(); j++) {
						sale = sales.get(j);
						String code = (String) sale.get("支店コード");
						if (code.equals(shitenCode)) {
							int uriage = (int) sale.get("売上金額");
							sale.put("売上金額", uriage + kingaku);
							break;
						}
					}

				} catch (IOException e) {
					System.out.println("エラーが発生しました。");
				} finally {
					if (br != null) {
						try {
							br.close();
						} catch (IOException e) {
							System.out.println("closeできませんでした。");
						}
					}
				}
			}
		}
	}

	// // チェックのためにコンソール出力
	// for(int i = 0; i < sales.size(); i++) {
	// System.out.println(sales.get(i));
	// }

	/*-**********************************************************
	  集計結果をファイルに出力する
	***********************************************************/
	BufferedWriter bw = null;try
	{
		dirPass = args[1]; // コマンドライン引数（支店別集計ファイル出力先）
		file = new File(dirPass, "branch.out");
		FileWriter fw = new FileWriter(file);
		bw = new BufferedWriter(fw);

		for (int i = 0; i < sales.size(); i++) {
			sale = sales.get(i);
			bw.write(sale.get("支店コード") + "," + sale.get("支店名") + "," + sale.get("売上金額") + "\r\n");
		}
	}catch(
	IOException e)
	{
		System.out.println("エラーが発生しました。");
	}finally
	{
			if(bw != null) {
				try {
					bw.close();
				} catch(IOException e) {
					System.out.println("closeできませんでした。");
				}
			}
		}
	}

	public static boolean isNumber(String num) {
		try {
	        Integer.parseInt(num);
	        return true;
	        } catch (NumberFormatException e) {
	        return false;
	    }
//	}
//}
