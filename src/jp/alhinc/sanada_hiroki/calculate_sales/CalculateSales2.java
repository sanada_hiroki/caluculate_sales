package jp.alhinc.sanada_hiroki.calculate_sales2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales2 {
	public static void main (String[] args) {
		//*****************************************
		//  支店定義ファイル読み込み
		//*****************************************
		BufferedReader br = null;
		Map<String, String> branchNameMap = new HashMap<String, String>();	// key=支店コード、value=支店名のMap
		Map<String, Long> branchSumMap = new HashMap<String, Long>();		// key=支店コード、value=売上合計金額のMap

		try {
			// コマンドライン引数が1つ以外のとき例外処理
			if(args.length != 1){
				System.out.println("予期せぬエラーが発生しました");
				return;
			}
			String dirPass;			// 支店定義ファイルのディレクトリパス
			dirPass = args[0];		// コマンドライン引数使用
			String line;			// 読み込んだlstファイルを１行を入れる変数
			File file = new File(dirPass, "branch.lst");
			// 支店定義ファイルが存在しないとき例外処理
			if(file.exists() == false) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			//支店定義ファイルを１行ずつ読み込んでMAPに格納
			while((line = br.readLine()) != null) {
				String[] data = line.split(",",0);	// 行をカンマ区切りで配列に
				//  読み込んだ１行に対してのフォーマットチェック
				//	要素数は２つ、かつ、支店コードが数字３桁
				if(data.length != 2 || data[0].matches("^\\d{3}$") == false){
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				// MAPに格納する
				branchNameMap.put(data[0], data[1]);
				branchSumMap.put(data[0], 0L);
			}

			//*****************************************
			//  集計処理
			//*****************************************
			// 売上ファイルを検索し、リストを取得
			File dir = new File(dirPass);
			File[] allFiles = dir.listFiles();
			List<String> files = new ArrayList<String>();
			String fileName;
			String fileExtension = ".rcd";
			String strFileName;

			//  指定された拡張子、かつ、先頭8字が半角数字のとき
			//  "数字8桁.rcd"だけのファイル名リストを作る
			for(int i = 0; i < allFiles.length; i++){
				fileName = allFiles[i].getName();
				if(fileName.length() == 12) {
					strFileName = fileName.substring(0, 8);
					if(allFiles[i].isFile() && fileName.endsWith(fileExtension) && strFileName.matches("^\\d{8}$")){
						files.add(fileName);
					}
				}
			}
			// ファイル名のフォーマットが正しい数字8桁かつ.rcd拡張子のファイルリストに対して行う処理
			// ファイル名が1から始まる連番ではないときは例外処理
			Collections.sort(files);
			int count = 1;
			String strFileNameNum;
			for(int i = 0; i < files.size(); i++) {
				br = null;
				try{
					fileName = files.get(i);
					strFileNameNum = fileName.substring(0, 8);
					// ファイル名連番チェックの例外処理
					if(Integer.parseInt(strFileNameNum) != count) {
						System.out.println("売上ファイル名が連番になっていません");
						return;
					}
					file = new File(dirPass, fileName);
					fr = new FileReader(file);
					br = new BufferedReader(fr);
					String branchCode = br.readLine();
					String strKingaku = br.readLine();
					Long kingaku;
					if( strKingaku.matches("^[0-9]*$")){
						kingaku = Long.parseLong(branchCode);
					}
					else{
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
					long sum = 0L;
					long max = 9999999999L;
					// 売上ファイルの中身が３行以上もしくは、10桁以上の場合の例外処理
					if( br.readLine() != null || kingaku > max ){
						System.out.println(files.get(i) + "のフォーマットが不正です");
						return;
					}
					// 支店コードの合計金額に加算
					sum = branchSumMap.get(branchCode) + kingaku;
					// 売上合計が10桁を超えた場合の例外処理
					if( sum > max ){
						System.out.println("合計金額が10桁を超えました");
						return;
					}
					branchSumMap.put(branchCode, sum);
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				} finally {
					if(br != null) {
						try {
							br.close();
						} catch(IOException e) {
							System.out.println("予期せぬエラーが発生しました");
							return;
						}
					}
				}
				count++;
			}
			//*****************************************
			//  出力
			//*****************************************
			List<String> strLine = new ArrayList<String>();
			// ファイルに出力
			for(Map.Entry<String, String> e : branchNameMap.entrySet())
				strLine.add((e.getKey() + "," + e.getValue() + "," + branchSumMap.get(e.getKey()) + System.lineSeparator()));
			if(writeBranchOut(dirPass + File.separator + "branch.out", strLine) == false ){
				System.out.println("予期せぬエラーが発生しました");
				return;
			}
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}
	}

	public static Boolean writeBranchOut(String strFilePass, List<String> strLine){
		BufferedWriter bw = null;
		try{
			File file = new File(strFilePass);
			bw = new BufferedWriter(new FileWriter(file));
			for( int i = 0; i < strLine.size(); i++ )
				bw.write(strLine.get(i));
		}catch (IOException e){
			return false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}